import React, { Component } from 'react';
import './styles/App.css';
import Homepage from './containers/Homepage';
import {
  BrowserRouter as Router,
  Switch,
  Route 
} from "react-router-dom";
import Signin from './containers/Signin';
import Signup from './containers/Signup';
import PanelAdmin from './containers/PanelAdmin';
import EditElement from './components/PanelAdmin/EditElement';
import Post from './components/PostPage/Post';

class App extends Component {
  render() { 
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Homepage} />
            <Route path="/signin" component={Signin} />
            <Route path="/signup" component={Signup} />
            <Route path="/paneladmin" component={PanelAdmin} />
            <Route path="/editelement" component={EditElement} />
            <Route path="/post" component={Post} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
