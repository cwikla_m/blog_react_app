const minMaxSize = {
    minMobile: '250px',
    minTablet: '500px',
    minDesktop: '1600px',
    maxMobile: '499px',
    maxTablet: '1599px',
    maxDesktop: '3000px'
  }

const size = {
    mobile: 250,
    tablet: 500,
    desktop: 1600
}


  
export const device = {
    minMobile: `(min-width: ${minMaxSize.minMobile})`,
    minTablet: `(min-width: ${minMaxSize.minTablet})`,
    minDesktop: `(min-width: ${minMaxSize.minDesktop})`,
    maxMobile: `(max-width: ${minMaxSize.maxMobile})`,
    maxTablet: `(max-width: ${minMaxSize.maxTablet})`,
    maxDesktop: `(max-width: ${minMaxSize.maxDesktop})`
  };

  export const getWidth = () => {
    if(window.innerWidth >= size) return 
  }
  
  
  
  