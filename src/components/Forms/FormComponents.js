import styled from 'styled-components';

export const Button = styled.button`
    margin: 5px 0px 10px 0px;
    width: 100%;
    height: 30px;
    background-color: #ccc;
    border: 1px solid black;
    color: black;
    font-wight: bold;
    border-radius: 5px;
    &:hover {
        box-shadow: 0px 0px 3px red;
        color: black;
    }
`;

export const Header = styled.h1`
    font-family: 'Open Sans', sans-serif;
    font-size: 24px;
    font-weight: bold;
    text-shadow: 0px 0px 3px red;
`;

export const Label = styled.label`
    margin: 0px;
    font-family: 'Public Sans', sans-serif;
    text-align: left;
`;

export const LabelError = styled.label`
    font-size: 12px;
    text-align: center;
    color: #ff3333;
    letter-spacing: 1px;
`;