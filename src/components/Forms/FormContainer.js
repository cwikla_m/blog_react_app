import React from 'react';
import './../../styles/dashboardStyle.css';
import styled from 'styled-components';


const FormContainer = ({ children }) => {
    return (
        <div>
            <ContainerBox>
                <Container>
                   {children} 
                </Container>
            </ContainerBox>
        </div>
    );
}

const ContainerBox = styled.div`
    padding: 80px 15px 75px 15px;
`;

const Container = styled.div`
    padding: 20px;
    display: inline-block;
    vertival-align: middle;
    max-width: 350px;
    width: 100%;
    height: auto;
    background-color: #404040;
    box-shadow: 0px 0px 4px black;
    color: #eee;
    font-family: 'Public Sans', sans-serif;
`;

export default FormContainer;