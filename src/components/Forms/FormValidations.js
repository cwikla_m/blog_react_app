const required = value => (value ? undefined : " Field is required");

const composeValidators = (...validators) => value => validators.reduce((error, validator) => error || validator(value), undefined);


export {composeValidators, required };