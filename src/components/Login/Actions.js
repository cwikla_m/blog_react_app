import Types from './Types';

const add = item => ({
    type: Types.ADD_LOGIN, item
})

const remove = item => ({
    type: Types.REMOVE_LOGIN, item
})

  export default {
      add,
      remove
  }