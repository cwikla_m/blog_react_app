import Types from './Types';

//Initial state value
const initialLogin = {
    Items: {
        userName: sessionStorage.getItem('userName'),
        token: sessionStorage.getItem('token'),
        admin: sessionStorage.getItem('admin')
    }
}

//Reducer
const loginReducer = (state = initialLogin, action) => {
    switch (action.type) {
        case Types.ADD_LOGIN:
            return {
                ...state, Items: {
                    ...state.Items,
                    userName: action.item.userName,
                    token: action.item.token,
                    admin: action.item.admin
                }
            }
        case Types.REMOVE_LOGIN:
            return {
                ...state, Items: {
                    userName: '',
                    token: '',
                    admin: false               
                }
            }
        default: 
            return state;
    }
}
//s
export default loginReducer;