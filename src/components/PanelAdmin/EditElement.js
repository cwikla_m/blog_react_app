import React, { Component } from 'react';
import Dashboard from '../../containers/Dashboard';
import { Formik } from 'formik';
import {  Form, TextArea, Input, MyButton, Label, Header, Container, ContainerBox, ErrorTxt } from './PanelAdminComponents';
import Actions from '../Posts/Actions';
import { connect } from 'react-redux';
import NotAdmin from './NotAdmin';
import { Alert, Spinner } from 'reactstrap';



const mapDispatchToProps = dispatch => ({
    //add: (post) => dispatch(Actions.add(post)),
    update: (post) => dispatch(Actions.update(post))
  })


const mapStateToProps = state => ({
    title: state.posts.Items.titles,
    content: state.posts.Items.contents,
    login: state.login.Items
})


class EditElement extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: null,
            isFetchingEdit: false,
            isFetchingGet: false,
            responseEdit: '',
            responseErrGet: '',
            responseErrEdit: '',
            alertEditIsOpen: false,
            datas: []
        }
    }

    componentWillMount() {
        this.getPost(this.readIdPost());
        
    }

    readIdPost = () => {
        let pathname = this.props.location.pathname;
        let idValue = parseInt(pathname.substr(13,pathname.length-1));
        console.log('idV: ', idValue);
        this.setState({ id: idValue })
        return idValue
    }

    checkAdmin = () => {
        const login = this.props.login;
        if(login.admin) return true
        else return false
    }

    getPost = async(id) => {
        this.setState({ responseErrGet: '', responseErrGet: false })
        await fetch(`https://localhost:5001/Posts/id=${id}`)
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);
            }
            else {
                return response;
        }
        })
        .then(json => json.json())
        .then(data => this.setState({ datas: data, isFetchingGet: true }))
        .catch((e) => this.setState({responseErrGet: e.message, isFetchingGet: true }))
    }

    editPost = async(values) => {
        this.setState({isFetchingEdit: false, responseErrEdit: false, alertEditIsOpen: false})
        await fetch('https://localhost:5001/Posts', {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.login.token}`,
            },
            body: JSON.stringify({
                "title": values.title,
                "content": values.content,
                "id_post": this.state.id,
            }),
        })
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);
            }
            else {
                this.setState({responseEdit: 'Post editted succesfully ', alertEditIsOpen: true, isFetchingEdit: true});
        }
        })
        .catch((e) => this.setState({responseErrEdit: e.message, isFetchingEdit: true, alertEditIsOpen: true}))

    }

    renderPost() {
        return(
            <div>
                <Header>Edit element</Header>
                        <Formik 
                                initialValues={{ title: this.state.datas.title, content: this.state.datas.content}}
                                onSubmit={ async (values) => {
                                    this.editPost(values)
                                }}
                                validate={(values) => {
                                    let errors = {};
    
                                    if(!values.title) errors.title = 'Title is required!';
                                    if(!values.content) errors.content = 'Content is required!';
    
                                    return errors;
                                }}
                                render={({
                                    values,
                                    errors,
                                    touched,
                                    handleBlur,
                                    handleChange,
                                    handleSubmit,
                                    isSubmitting
                                }) => (
                                    <Form onSubmit={handleSubmit}>
                                        <Label>Title: </Label>
                                        <Input 
                                            name='title' 
                                            onChange={handleChange}
                                            value={values.title}
                                            maxLength='70'
                                            />
                                        <ErrorTxt>{errors.title}</ErrorTxt>
                                        <Label>Content: </Label>
                                        <TextArea 
                                            name='content' 
                                            type='password'
                                            onChange={handleChange}
                                            value={values.content}
                                            />
                                        <ErrorTxt>{errors.content}</ErrorTxt>
                                        {
                                            this.state.isFetchingEdit ? this.state.responseErrEdit ? 
                                            <Alert 
                                                color="danger" 
                                                isOpen={this.state.alertEditIsOpen} 
                                                toggle={this.onDismiss}>
                                                    {this.state.responseErrEdit}   
                                            </Alert> 
                                            : 
                                            <Alert 
                                                color="success" 
                                                isOpen={this.state.alertEditIsOpen} 
                                                toggle={this.onDismiss}>
                                                    {this.state.responseEdit}
                                            </Alert> 
                                            :
                                            undefined
                                        }
                                        <MyButton type='submit' >Save</MyButton>
                                    </Form>
                                )
                            }
                            />
            </div>
        )
    }

    renderEditElement() {
        const id = this.state.id;
        const titleProp = this.props.title[id];
        const contentProp = this.props.content[id];
        console.log('tit: ', titleProp, 'cont: ', contentProp );
        return(
            <Dashboard>
                <ContainerBox>
                    <Container>
                        {
                            !this.state.isFetchingGet
                            ?
                            <Spinner style={{ width: '3rem', color: 'primary', height: '3rem' }} />
                            :
                            this.renderPost()
                        }
                        
                    </Container>
                </ContainerBox>      
            </Dashboard>
            
        );
    }

    renderGoLogin() {
        return(
            <NotAdmin />
        );
    }

    render() {
        return(
            <div>
                {this.checkAdmin() ? this.renderEditElement() : this.renderGoLogin()}
            </div>  
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (EditElement);