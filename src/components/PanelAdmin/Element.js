import React, { Component } from 'react';
import styled from 'styled-components';
import { FaTrash, FaEdit, FaWindowClose } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import Actions from '../Posts/Actions';
import { connect } from 'react-redux';
import { device } from './../FluidStyles/Device';
import { Popover, PopoverHeader } from 'reactstrap';

const mapDispatchToProps = dispatch => ({
    //add: (post) => dispatch(Actions.add(post)),
    remove: (post) => dispatch(Actions.remove(post))
  })


const mapStateToProps = state => ({
    title: state.posts.Items.titles,
    content: state.posts.Items.contents,
    id_post: state.posts.Items.id_posts,
    login: state.login.Items
})



class Element extends Component {

    state = {
        response: '',
        popoverIsOpen: false
    }
    
    removePost = async() => {
        this.setState({ popoverIsOpen: false })
        await fetch('https://localhost:5001/Posts', {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.login.token}`,
            },
            body: JSON.stringify({
                'id_post': this.props.id_post[this.props.id],
            }),
        })
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Status code: ' + response.status);
            }
            else {
                this.props.update();
        }
        })
        .catch((e) => this.setState({response: e.message, popoverIsOpen: true}))
        //this.resetInput(values);
    }

    handlePopover = () => {
        this.setState({popoverIsOpen: false});
    }
    

    render() {
        let initialValue = {
            title: '',
            id: this.props.id,
            content: ''
        }
        return(
            <Container>
                {this.props.title[this.props.id]}
                <div>
                    <Link to={`/editelement/${this.props.id_post[this.props.id]}` }> <FaEdit className='icon' color='#ccc' /> </Link> 
                    <FaTrashStyle className='icon' color='#ccc' id='popover' onClick={(e) => this.removePost(e, initialValue)} /> 
                    <Popover placement="bottom" isOpen={this.state.popoverIsOpen} target="popover">
                        <PopoverHeaderStyle>Error: {this.state.response} <FaWindowClose onClick={this.handlePopover} /></PopoverHeaderStyle>
                        
                    </Popover>  
                </div>
                
            </Container>
        );
    }
}

const Container = styled.div`
    margin: 5px 0px;
    border-radius: 10px;
    padding: 10px 15px;
    min-height: 40px;
    display: flex;
    flex-direction: row;
    text-align: left;
    justify-content: space-between;
    color: #ccc;
    background-color: #707070;
    @media ${device.maxDesktop} {
        font-size: 16px;
    }
    @media ${device.maxTablet} {
        font-size: 12px;
    }
    @media ${device.maxMobile} {    
        font-size: 10px;
    }
`;

const PopoverHeaderStyle = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 5px;

`;

const FaTrashStyle = styled(FaTrash)`
    &:hover {
        cursor: pointer;
    }
`;

export default connect(mapStateToProps, mapDispatchToProps) (Element);