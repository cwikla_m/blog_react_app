import React  from 'react';
import {  ContainerCenter, BoxNotLogin, TextNotLogin } from './PanelAdminComponents';
import police from './../../img/police.png';
import { Link } from 'react-router-dom';

const NotAdmin = () => {
    return(
        <ContainerCenter>
            <BoxNotLogin>
                <img src={police} alt='Image error' />
                <TextNotLogin>You are not an Admin!! Let's <Link className='linkForm' to='/signin'> Sign in</Link></TextNotLogin>
            </BoxNotLogin>
    </ContainerCenter> 
    );
    
}
     
export default NotAdmin
