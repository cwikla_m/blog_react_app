import './../../styles/dashboardStyle.css';
import styled from 'styled-components';
import { device } from './../FluidStyles/Device';

const Container = styled.div`
    padding: 0px 10px;
    display: inline-block;
    vertival-align: middle;
    max-width: 700px;
    width: 100%;
    height: auto;
    background-color: #404040;
    box-shadow: 0px 0px 2px #909090;
    color: #eee;
    font-family: 'Public Sans', sans-serif;
`;

const ContainerBox = styled.div`
    padding: 80px 15px 75px 15px;
`;

const Header = styled.h1`
    margin: 15px 0px;
    color: #ddd;
    font-family: 'Poppins', sans-serif;
    font-size: 18px;
    @media ${device.maxDesktop} {
        font-size: 38px;
    }
    @media ${device.maxTablet} {
        font-size: 24px;
    }
    @media ${device.maxMobile} {
        font-size: 16px;
    }
`;

const Label = styled.label`
    margin: 5px 10px;
    font-family: 'Public Sans', sans-serif;
    text-align: left;
    color: #ddd;
`;

const MyButton = styled.button`
    width: 100px;
    height: 40px;
    margin: 15px 5px;
    color: #ddd;
    background-color: #707070;
    border: solid 2px #555;
    border-radius: 8px;
    &:hover {
        box-shadow: 0px 0px 2px #ccc;
        color: white;
    }
`;

const Input = styled.input`
    width: 100%;
    min-height: 40px;
    padding: 0px 10px;
    margin: 5px 0px;
    background-color: #707070;
    color: #ccc;
    border: none;
    border-radius: 7px;
`;

const TextArea = styled.textarea`
    width: 100%;
    padding: 0px 10px;
    margin: 5px 0px;;
    height: 400px;
    background-color: #707070;
    color: #ccc;
    border: none;
    border-radius: 10px;
`;

const Form = styled.form`
    align-items: center;
`;

const ErrorTxt = styled.p`
    font-size: 16px;
    text-align: center;
    color: #ff3333;
    letter-spacing: 1px;
`

const ContainerCenter = styled.div`
    height: 100vh;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    padding: 0px 5px;
`;

const BoxNotLogin = styled.div`
    padding: 0px 10px;
    display: inline-block;
    vertival-align: middle;
    width: 100%;
    max-width: 400px;
    height: auto;
    background-color: #333;
    border-radius: 10px;
    border: 3px solid red;
    box-shadow: 0px 0px 2px red;
    color: #eee;
    font-family: 'Public Sans', sans-serif;
`;

const TextNotLogin = styled.div`
    font-family: 'Public Sans', sans-serif;
    font-weight: bold;
    color: white;
    text-shadow: 0px 0px 2px black;
`;

export { Form, TextArea, Input, MyButton, Label, Header, Container, ContainerBox, ErrorTxt, ContainerCenter, BoxNotLogin, TextNotLogin }