import React, { Component } from 'react';
import { FaUserAlt } from 'react-icons/fa';
import styled from 'styled-components';
import { device } from './../FluidStyles/Device';
import { Formik } from 'formik';
import { Alert } from 'reactstrap';
import { Form, TextArea, MyButton, ErrorTxt } from './../PanelAdmin/PanelAdminComponents';

class CommentElement extends Component {

    state = {
        isEditComment: false,
        responseEdit: '',
        responseDelete: '',
        responseEditErr: '',
        responseDeleteErr: '',
        alertEditIsOpen: false,
        alertDeleteIsOpen: false,
        isFetchingEdit: false,
        isFetchingDelete: false
    }

    editComment = async(values) => {
        await fetch('https://localhost:5001/Comments', {
             method: 'PUT',
             headers: {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json',
                 'Authorization': `Bearer ${this.props.token}`
             },
             body: JSON.stringify({
                 "id_post": this.props.id_post,
                 "id_comment": this.props.id_comment,
                 "content": values.content
             }),
         })
         .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);
            }
            else {
                this.props.update()
        }
        })
         .catch((e) => {
             console.log('err: ', e);
            this.setState({responseEdit: e.message, isFetchingEdit: true ,responseEditErr: true, alertEditIsOpen: true});
         })
         
    }

    deleteComment = async() => {
        await fetch('https://localhost:5001/Comments', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.props.token}` 
            },
            body: JSON.stringify({
                "id_post": this.props.id_post,
                "id_comment": this.props.id_comment
            }),
        })
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);
            }
            else {
                this.props.update()
        }
        })
         .catch((e) => {
            this.setState({responseDelete: e.message, isFetchingDelete: true ,responseDeleteErr: true, alertDeleteIsOpen: true});
         })
    }

    onDismiss = () => {
        this.setState({alertEditIsOpen: false, alertDeleteIsOpen: false})
    }
    
    renderEditComment() {
        return (
            <div>
                <Formik 
                    initialValues={{ content: this.props.content}}
                    onSubmit={ async (values) => {
                        console.log('comment: ' + values.content);
                        this.editComment(values);
                        
                        }}
                        validate={(values) => {
                            let errors = {};

                            if(!values.content) errors.content = 'Type comment!';

                            return errors;
                        }}
                        render={({
                            values,
                            errors,
                            touched,
                            handleBlur,                                
                            handleChange,
                            handleSubmit,
                            isSubmitting
                        }) => (
                            <Form onSubmit={handleSubmit}>
                                <TextArea 
                                    name='content' 
                                    onChange={handleChange}
                                    value={values.content}
                                    />
                                <ErrorTxt>{errors.content}</ErrorTxt>
                                {
                                    this.state.isFetchingEdit ? this.state.responseEditErr ? 
                                        <Alert 
                                            color="danger" 
                                            isOpen={this.state.alertEditIsOpen} 
                                            toggle={this.onDismiss}>
                                                {this.state.responseEdit}   
                                        </Alert> 
                                    : 
                                    undefined
                                    :
                                    undefined
                                }
                                <MyButton type='submit' >Save</MyButton>
                                <MyButton onClick={this.toggleEditComment}>Cancel</MyButton>
                            </Form>            
                        ) 
                    }
                    />            
            </div>
        )
    }

    toggleEditComment = () => {
        this.setState({isEditComment: !this.state.isEditComment})
    }

    renderButtons() {
        return(
            <EditDelete>
                <CommentButton onClick={this.toggleEditComment}>edit</CommentButton>
                <CommentButton onClick={this.deleteComment}>delete</CommentButton>
            </EditDelete>
            
        )
    }

    render() {
        return(
            <Container>
                <FaUserAlt color='white' className='userIcon' size='15' min-size='10' />
                <CommentContainer>
                    <UserAndDate>
                        <UserName>{this.props.login}</UserName>
                        <Date>{this.props.date}</Date>
                        {
                            (this.props.userName === this.props.login) 
                            ? 
                            this.renderButtons()
                            : 
                            undefined 
                        }   
                    </UserAndDate>   
                    <Comment>{this.props.content}</Comment>
                    {
                        this.state.isFetchingDelete ? this.state.responseDeleteErr ? 
                            <Alert 
                                color="danger" 
                                isOpen={this.state.alertDeleteIsOpen} 
                                toggle={this.onDismiss}>
                                    {this.state.responseDelete}   
                            </Alert> 
                        : 
                        undefined
                        :
                        undefined
                    }
                    {
                        this.state.isEditComment
                        ?
                        this.renderEditComment()
                        :
                        undefined
                    }  
                </CommentContainer>
                    
            </Container>
        );
    }
}

const CommentContainer = styled.div`
    width: auto;
`;

const UserAndDate = styled.div`
    display: flex;
    justify-content: flex-start;
    @media ${device.maxMobile} {
        display: block;
        align-items: left;
        text-align: left;
    }
`;

const EditDelete = styled.div`
    display: flex;
`;

const Date = styled.div`
    font-wieght: bold;
    color: #bbb;
    @media ${device.maxDesktop} {
        font-size: 20px;
    }
    @media ${device.maxTablet} {
        font-size: 16px;
    }
    @media ${device.maxMobile} {
        font-size: 12px;
        margin-left: 8px;
    }
`;

const Comment = styled.div`
    margin: 10px 0px;
    float: left;
    text-align: left;
    @media ${device.maxDesktop} {
        font-size: 16px;
    }
    @media ${device.maxTablet} {
        font-size: 12px;
    }
    @media ${device.maxMobile} {
        font-size: 10px;
    }
`;

const UserName = styled.div`
    font-family: 'Calistoga';
    letter-spacing: 1px;
    margin: 0px 8px;
    @media ${device.maxDesktop} {
        font-size: 22px;
    }
    @media ${device.maxTablet} {
        font-size: 18px;
    }
    @media ${device.maxMobile} {
        font-size: 14px;
    }
`;

const Container = styled.div`
    display: flex;
    width: 100%;
    margin-top: 10px;
    border-bottom: 2px solid #ccc;
`;

const CommentButton = styled.div`
    margin: 5px;

    color: #4287f5;
    @media ${device.maxDesktop} {
        font-size: 16px;
    }
    @media ${device.maxTablet} {
        font-size: 12px;
    }
    @media ${device.maxMobile} {
        font-size: 10px;
    }
    &:hover {
        color: #62a7ff;
        cursor: pointer;
    }
`;


export default CommentElement;