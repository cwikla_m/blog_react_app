const replaceEnterChar = (initialString) => {
    let value = [];
    let check = /\n/;
    let doIt = true;
    let i = 0;

    let enterPosition = initialString.search(check);
    while (doIt) { 
        if(enterPosition !== -1){
            value[i] = initialString.substr(0, enterPosition)
            let dl = initialString.length - 1;
            initialString = initialString.substr(enterPosition + 1, dl)
            enterPosition = initialString.search(check);
            i++
        }
        else {
            doIt = false;
        } 
    }
    value[i] = initialString.substr(0, initialString.length);
    return value
    }

    export { replaceEnterChar };