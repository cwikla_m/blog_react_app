import React, { Component } from 'react';
import styled from 'styled-components';
import Actions from '../Posts/Actions';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { replaceEnterChar } from './Functions';
import { device } from './../FluidStyles/Device';
import { Formik } from 'formik';
import { FaPlus } from 'react-icons/fa';
import {  Form, TextArea, MyButton, Header, ErrorTxt } from './../PanelAdmin/PanelAdminComponents';
import CommentElement from './CommentElement';
import { Spinner, Alert } from 'reactstrap';


const mapDispatchToProps = dispatch => ({
    //add: (post) => dispatch(Actions.add(post)),
    remove: (post) => dispatch(Actions.remove(post))
  })


const mapStateToProps = state => ({
    id_posts: state.posts.Items.id_posts,
    titles: state.posts.Items.titles,
    contents: state.posts.Items.contents,
    dates: state.posts.Items.dates,
    login: state.login.Items
})

class Post extends Component {
    constructor(props){
        super(props);
        this.state = {
            id: null,
            isLoadingComment: true,
            isLoadingPost: true,
            isErrorAddComment: false,
            datasComments: [],
            dataPost: [],
            responseGetPostErr: '',
            responseGetCommentsErr: '',
            responseAddCommentErr: '',
            update: true
        }
        this.updateComments = this.updateComments.bind(this);
    }


    componentWillMount() {
        console.log('path: ', this.props.location.pathname);
        this.readIdPost();
    }

    updateComments() {
        this.getComments();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        console.log('idpost: ', this.state.id)
        this.getPost();
        this.getComments();
    }

    readIdPost = () => {
        let pathname = this.props.location.pathname;
        let idValue = parseInt(pathname.substr(6,pathname.length-1));
        this.setState({
            id: idValue
        })
    }

    resetInput = (value) => {
        value.content = '';
        return value;
    }

    getPost = async() => {
        this.setState({ isLoadingPost: true })
        await fetch(`https://localhost:5001/Posts/id=${this.state.id}`)
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);             
            }
            else {
                return response;
        }
        })
        .then(json => json.json())
        .then(data => {
            console.log('dataPost: ', data);
            this.setState({ dataPost: data, isLoadingPost: false });
        })
        .catch((e) => this.setState({responseGetPostErr: 'Failed to load Post: ' + e.message, isLoadingPost: false }))
    }

    getComments = async() => {
        this.setState({ isLoadingComment: true })
        await fetch(`https://localhost:5001/Comments/id_post=${this.state.id}`)
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);             
            }
            else {
                return response;
        }
        })
        .then(json => json.json())
        .then(data => {
            this.setState({ datasComments: data, isLoadingComment: false });
        })
        .catch((e) => this.setState({responseGetCommentsErr: 'Failed to load comments: ' + e.message, isLoadingComment: false }))
    }

    
    addComment = async(values) => {
        this.setState({ isErrorAddComment: false })
        await fetch('https://localhost:5001/Comments', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.login.token}`
            },
            body: JSON.stringify({
                "content": values.content,
                "login": this.props.login.userName,
                "id_post": this.state.id,
            }),
        })
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);  
            }
            else {
                this.getComments();
        }
        })
        .catch((e) => this.setState({ responseAddCommentErr: e.message, isErrorAddComment: true }))
        this.resetInput(values);
    }
    
    renderPost() {
        return(
            <div>
                <ContainerTitle>
                    <Title>{this.state.dataPost.title}</Title>  
                </ContainerTitle>
                <ContainerContent>
                    <Date>{this.state.dataPost.datetime.substr(0,10)}</Date>
                    {replaceEnterChar(this.state.dataPost.content).map((val) => 
                        <ContentValue>{val}</ContentValue>
                    )}   
                </ContainerContent>
            </div>
        )
    }

    onDismissAlertAddComment() {
        this.setState({ isErrorAddComment: false     })
    }

    render() {
        return(
            <Container>
                <ContainerBox>
                    {
                    this.state.isLoadingPost 
                    ? 
                    <Spinner style={{ width: '3rem', color: 'primary', height: '3rem' }} />
                    :
                    this.state.responseGetPostErr
                    ?
                    <ErrorTxt>Error: {this.state.responseGetPostErr}</ErrorTxt>
                    :
                    this.renderPost()
                    }
                    
                    <CommentContainer>
                        {
                            this.state.isLoadingComment 
                            ?
                            <Spinner style={{ width: '3rem', color: 'primary', height: '3rem' }} />
                            :
                            this.state.responseGetCommentsErr
                            ?
                            <ErrorTxt>Error: {this.state.responseGetCommentsErr}</ErrorTxt>
                            :
                            this.state.datasComments.map(item => 
                                <CommentElement
                                    userName = {this.props.login.userName}
                                    content = {item.content}
                                    login = {item.login}
                                    date = {item.datetime.substr(0,10)}
                                    id_comment = {item.id_comment}
                                    id_post = {this.state.id}
                                    token = {this.props.login.token}
                                    update = {this.updateComments}
                            />
                            )
                        }
                        {
                            !this.props.login.userName
                            ?
                            <Header2>Do you want add a comment? <Link className='linkForm' to='/signin'> Sign in</Link></Header2>
                            :
                            <div>
                                <Header>Add comment</Header>
                        <Formik 
                            initialValues={{ content: ''}}
                            onSubmit={ async (values) => {
                                console.log('comment: ' + values.content);
                                this.addComment(values);     
                            }}
                            validate={(values) => {
                                let errors = {};

                                if(!values.content) errors.content = 'Type comment!';

                                return errors;
                            }}
                            render={({
                                values,
                                errors,
                                touched,
                                handleBlur,
                                handleChange,
                                handleSubmit,
                                isSubmitting
                            }) => (
                                <Form onSubmit={handleSubmit}>
                                    <TextArea 
                                        name='content' 
                                        onChange={handleChange}
                                        value={values.content}
                                        />
                                    <ErrorTxt>{errors.content}</ErrorTxt>
                                    {
                                        <Alert 
                                            color="danger" 
                                            isOpen={this.state.isErrorAddComment} 
                                            toggle={this.onDismissAlertAddComment}>
                                                {this.state.responseAddCommentErr}   
                                        </Alert> 
                                    }
                                    <MyButton type='submit' ><FaPlus color='white' size='20' /></MyButton>
                                </Form>            
                            ) 
                        }
                        />
                            </div>
                        }
                        
                    </CommentContainer>
                </ContainerBox>
            </Container>
                
            
        );
    }
}

const Container = styled.div`
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 25px;
`;

const ContainerBox = styled.div`
    margin-top: 30px;
    padding: 10px 10px;
    display: inline-block;
    vertival-align: middle;
    max-width: 1000px;
    width: 100%;
    height: auto;
    background-color: #404040;
    opacity: 95%;
    box-shadow: 0px 0px 2px #909090;
    color: #eee;
    border-radius: 5px;
    font-family: 'Public Sans', sans-serif;
`;

const ContainerTitle = styled.div`
    margin: 5px auto;
    padding: 10px 15px;
    min-height: 40px;
    display: flex;
    align-items: center;
    justify-content: left;
    color: #ddd;
`;

const CommentContainer = styled.div`
    padding: 5px 0px;
    max-width: 900px;
    margin: auto;
`;

const ContainerContent = styled.div`
    margin: 5x auto;
    margin-bottom: 50px;
    border-radius: 10px;
    padding: 10px 15px;
    display: block;
    text-align: left
    color: #eee;
    background-color: #707070;
    font-family: 'Calistoga';
    letter-spacing: 0.5px;
`;

const ContentValue = styled.div`
    text-indent: 1.5em;
    margin-bottom: 10px;
    text-align: left;
    @media ${device.maxDesktop} {
        font-size: 14px;
    }
    @media ${device.maxTablet} {
        font-size: 12px;
    }
    @media ${device.maxMobile} {
        font-size: 10px;
    }
`;


const Title = styled.div`
    margin: 0px 5px;
    margin-top: 8px;
    font-family: 'Calistoga';
    text-align: left;
    vertical-align: center;
    border-radius: 10px;
    @media ${device.maxDesktop} {
        font-size: 38px;
        padding: 0px 25px;
        line-height: 50px;
        border: solid 4px #707070;
    }
    @media ${device.maxTablet} {
        font-size: 24px;
        padding: 0px 15px;
        line-height: 30px;
        border: solid 3px #707070;
    }
    @media ${device.maxMobile} {
        font-size: 16px;
        padding: 0px 7px;
        line-height: 22px;
        border: solid 2px #707070;
    }
`;

const Date = styled.div`
    margin: 0px 3px;
    padding: 0px 5px;
    font-family: 'Calistoga';
    font-size: 10px;
    float: right;
    border-radius: 7px;
    border: solid 2px #404040;
`;

const Header2 = styled.div`
    font-family: 'Calistoga';
    letter-spacing: 1px;
    @media ${device.maxDesktop} {
        font-size: 22px;
    }
    @media ${device.maxTablet} {
        font-size: 18px;
    }
    @media ${device.maxMobile} {
        font-size: 12px;
    }
`;

export default connect(mapStateToProps, mapDispatchToProps) (Post);