import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { replaceEnterChar } from './Functions';
import { device } from './../FluidStyles/Device';

class PostElement extends Component {

    

    render() {
        return(
            <Container>
                <ContainerTitle>
                    <Title>{this.props.titleProp}</Title>                
                </ContainerTitle>
                <ContainerContent>
                    <Date>{this.props.dateProp}</Date>
                    <ContentValue>{replaceEnterChar(this.props.contentProp)[0]} [...] </ContentValue>
                    <LinkStyle to={`/post/${this.props.id}`}>Read more</LinkStyle>
                </ContainerContent>
               
               
            </Container>
        );
    }
}

const Container = styled.div`
`;

const ContainerTitle = styled.div`
    margin: 5px auto;
    padding: 10px 15px;
    min-height: 40px;
    display: flex;
    align-items: flex-end;
    justify-content: space-between;
    color: #ddd;
`;

const ContainerContent = styled.div`
    margin: 5x auto;
    margin-bottom: 50px;
    border-radius: 10px;
    padding: 10px 15px;
    display: block;
    text-align: right;
    color: #ddd;
    background-color: #707070;
    font-family: 'Calistoga';
    letter-spacing: 0.5px;
`;

const ContentValue = styled.div`
    text-indent: 1.5em;
    margin-bottom: 10px;
    text-align: left;
    @media ${device.maxDesktop} {
        font-size: 14px;
    }
    @media ${device.maxTablet} {
        font-size: 12px;
    }
    @media ${device.maxMobile} {
        font-size: 10px;
    }
`;


const Title = styled.div`
    margin: 0px 5px;
    margin-top: 8px;
    font-family: 'Calistoga';
    text-align: left;
    vertical-align: center;
    border-radius: 10px;
    @media ${device.maxDesktop} {
        font-size: 38px;
        padding: 0px 25px;
        line-height: 50px;
        border: solid 4px #707070;
    }
    @media ${device.maxTablet} {
        font-size: 24px;
        padding: 0px 15px;
        line-height: 30px;
        border: solid 3px #707070;
    }
    @media ${device.maxMobile} {
        font-size: 16px;
        padding: 0px 7px;
        line-height: 22px;
        border: solid 2px #707070;
    }
`;

const LinkStyle = styled(Link)`
    color: #4db8ff;
    margin-right: 5px;
    margin-bottom: 5px;
    &:hover {
        padding: 3px;
        color: #4db8ff;
        border-radius: 5px;
        border: solid 2px #4db8ff;
        text-decoration: none;
        margin-right: 0px;
        margin-bottom: 0px;
    }
    @media ${device.maxTablet} {
        font-size: 14px;
    }
    @media ${device.maxMobile} {
        font-size: 12px;
    }
`;

const Date = styled.div`
    margin: 0px 3px;
    padding: 0px 5px;
    font-family: 'Calistoga';
    font-size: 10px;
    float: right;
    border-radius: 7px;
    border: solid 2px #404040;
`;



export default PostElement;