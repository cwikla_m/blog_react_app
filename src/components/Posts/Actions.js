import Types from './Types';

const add = item => ({
    type: Types.ADD_POST, item
})

const reset = item => ({
    type: Types.RESET_POSTS, item
  })

const update = item => ({
    type: Types.UPDATE_POST, item
})

const remove = item => ({
    type: Types.REMOVE_POST, item
})

const newarr = item => ({
    type: Types.NEW_ARR, item
})

  export default {
      add,
      reset,
      update,
      remove,
      newarr
  }