import Types from './Types';

//Initial state value
const initialPosts = {
    Items: {
        id_posts: [],
        titles: [],     
        contents: [],
        dates: []
    }
}

//Reducer
const postsReducer = (state = initialPosts, action) => {
    //console.log('action.id', action.item.id);
    switch (action.type) {
        case Types.ADD_POST:
            return {
                ...state, Items: {
                    ...state.Items,
                    id_posts: [...state.Items.id_posts, action.item.id_post],
                    titles: [...state.Items.titles, action.item.title],
                    contents: [...state.Items.contents, action.item.content],
                    dates: [...state.Items.dates, action.item.date]
                }
            }
        case Types.RESET_POSTS:
            return {
                ...state, Items: {
                    id_posts: [],
                    titles: [],
                    contents: [],
                    dates: []                
                }
            }
            case Types.UPDATE_POST:
            return {
                ...state, Items: {
                    ...state.Items,
                    id_posts: [...state.Items.id_posts.slice(0, action.item.id), action.item.id_posts, ...state.Items.id_posts.slice(action.item.id + 1, action.item.length)],
                    titles: [...state.Items.titles.slice(0, action.item.id), action.item.title, ...state.Items.titles.slice(action.item.id + 1, action.item.length)],
                    contents: [...state.Items.contents.slice(0, action.item.id), action.item.content, ...state.Items.contents.slice(action.item.id + 1, action.item.length)],
                    dates: [...state.Items.dates.slice(0, action.item.id), action.item.date, ...state.Items.dates.slice(action.item.id + 1, action.item.length)]
                }
            }
            case Types.REMOVE_POST:
            return {
                ...state, Items: {
                    ...state.Items,
                    id_posts: [...state.Items.id_posts.slice(0, action.item.id), ...state.Items.id_posts.slice(action.item.id + 1, action.item.length)],
                    titles: [...state.Items.titles.slice(0, action.item.id), ...state.Items.titles.slice(action.item.id + 1, action.item.length)],
                    contents: [...state.Items.contents.slice(0, action.item.id), ...state.Items.contents.slice(action.item.id + 1, action.item.length)],
                    dates: [...state.Items.dates.slice(0, action.item.id), ...state.Items.dates.slice(action.item.id + 1, action.item.length)]
                }
            }
        default: 
            return state;
    }
}

export default postsReducer;