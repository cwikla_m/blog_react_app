import postsReducer from './Reducers';
export { default as postTypes } from './Types';
export { default as postActions } from './Actions';
export default postsReducer;