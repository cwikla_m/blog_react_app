import { combineReducers } from 'redux'
import postsReducer from './Posts/Reducers';
import loginReducer from './Login/Reducers';

const rootReducer = combineReducers({
    posts: postsReducer,
    login: loginReducer
})

export default rootReducer;