import React, { useState } from 'react';
import './../styles/dashboardStyle.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,  
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';  
import Actions from '../components/Login/Actions';
import { Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap'
import { FaUserAlt } from 'react-icons/fa';

const mapDispatchToProps = dispatch => {
    return{
        remove: () => dispatch(Actions.remove())
}  
  }

const mapStateToProps = state => {
    return {
        login: state.login.Items.userName
    }   
}


const Dashboard = ({ children, remove, login }) => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const toggleDropDown = () => setDropdownOpen(prevState => !prevState);

    const signout = () => {
        sessionStorage.clear();
        remove();
        
    }
    let token = sessionStorage.getItem('token');    

    return (
        <div className="containerApp">
            <Navbar className="navbar custom-toggler fixed-top" fixed-top expand="sm">
                <Link className="navbarLogo nav-link" to="/" >Tuning & Cars  </Link>
                <NavbarToggler color="white" onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <Link className="navbarText nav-link" to="/" >  News  </Link>
                        </NavItem>
                        <NavItem>
                            <Link className="navbarText nav-link" to="/signup" >  Sign up  </Link>
                        </NavItem>
                        {
                            token
                            ?
                            <Dropdown isOpen={dropdownOpen} toggle={toggleDropDown}>
                                <DropdownToggle
                                    tag="span"
                                    data-toggle="dropdown"
                                    aria-expanded={dropdownOpen}
                                >
                                    <Link className="navbarText nav-link" > <FaUserAlt /> {login} </Link>
                                </DropdownToggle>
                                <DropdownMenu className="dropdownmenu">
                                    <Link className="navbarText nav-link" onClick={signout} >  Sign out  </Link>
                                </DropdownMenu>
                                </Dropdown>
                            :
                            
                                <NavItem>
                                    <Link className="navbarText nav-link" to="/signin" >  Sign in  </Link>
                                </NavItem>                            
                        }
                        
                        
                    </Nav>
                </Collapse>
            </Navbar>
            {children}
            <footer className="footer text-center">
                <div className="container">
                    <p className="footerText"> © Developed for <a className="link" href="http://prosper24.pl/web/" target="_blank" >Prosper </a></p>
                    <p className="footerText"> <Link className="link" to="/paneladmin">Go to panel admin</Link></p>    
                </div>
            </footer>
        </div>
    );
}


export default connect(mapStateToProps, mapDispatchToProps) (Dashboard);