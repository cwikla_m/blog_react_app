import React, { Component } from 'react';
import styled from 'styled-components';
import Dashboard from './Dashboard';
import './../styles/homepageStyle.css';
import PostElement from '../components/PostPage/PostElement';
import Actions from '../components/Posts/Actions';
import { connect } from 'react-redux'
import RightImg from './../img/right_charger.png';
import LeftImg from './../img/left_charger.png';
import { Spinner, Alert } from 'reactstrap';

const mapDispatchToProps = dispatch => ({
    add: (post) => dispatch(Actions.add(post)),
    reset: () => dispatch(Actions.reset())
  })


const mapStateToProps = state => ({
    id_posts: state.posts.Items.id_posts,
    titles: state.posts.Items.titles,
    contents: state.posts.Items.contents,
    dates: state.posts.Items.dates
})


class Homepage extends Component {    
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            datas: [],
            getPostsErr: '',
            showAlert: false
        }
    }

    async componentDidMount() {
        this.setState({ isLoading: true })
        await fetch('https://localhost:5001/Posts')
            .then (response => {
                if(response.status !== 200){
                    throw new Error('Invalid status Code : ' + response.status)
                }
                else {
                    return response;
                }
            })
            .then (response => response.json())
            .then (data => this.setState({ isLoading: false, datas: data}))
            .catch(error => this.setState({ isLoading: false, getPostsErr: error.message, showAlert: true }))
            this.postsToRedux(this.state.datas);
    }
    
    postsToRedux = (data) => {
        this.props.reset();
        data.map((el) => {
            const value = {
                id_post: el.id_post,
                title: el.title,
                content: el.content,
                date: el.datetime.substr(0,10)
            }
            this.props.add(value);
        })
    }
    
    render() {
        return (
            <Dashboard>
                <ContainerBox>
                    <Container>
                        <ContainerTitle>
                            <Img src={LeftImg} alt="Left Img" />
                            <Header>News</Header>
                            <Img src={RightImg} alt="Right Img" />
                        </ContainerTitle> 
                        
                        {/*this.props.titles.map((title, index) => 
                            <PostElement titleProp={title} id={index} contentProp={this.props.contents[index]} />
                        )*/}
                        { 
                            this.state.isLoading
                            ?
                            <Spinner style={{ width: '3rem', color: 'primary', height: '3rem' }} />
                            :
                            this.state.getPostsErr
                            ?
                            <Alert 
                                color="danger" 
                                isOpen={this.state.showAlert}>
                                    {this.state.getPostsErr}   
                            </Alert>  
                            :
                            this.state.datas.map((data, index) => 
                                <PostElement 
                                    titleProp={data.title} 
                                    id={this.props.id_posts[index]} 
                                    contentProp={data.content}
                                    dateProp={data.datetime.substr(0,10)} />
                            )
                        }                    
                    </Container>
                </ContainerBox>
            </Dashboard>

        );

    }
    
}

const Container = styled.div`
    padding: 10px 10px;
    display: inline-block;
    vertival-align: middle;
    max-width: 700px;
    width: 100%;
    height: auto;
    background-color: #404040;
    opacity: 95%;
    box-shadow: 0px 0px 2px #909090;
    color: #eee;
    border-radius: 5px;
    font-family: 'Public Sans', sans-serif;
`;

const ContainerTitle = styled.div`
    margin: 15px auto;
    border-radius: 10px;
    padding: 10px 15px;
    min-height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #ddd;
`;

const ContainerBox = styled.div`
    padding: 80px 15px 75px 15px;
`;

const Header = styled.h1`
    font-size: 60px;
    margin: 0px 10px;
    font-family: 'Calistoga', cursive;
`;

const Img = styled.img`
    height: 70px;
    width: auto;
    margin-bottom: 8px;
`;


export default connect(mapStateToProps, mapDispatchToProps) (Homepage);


