import React, { Component } from 'react';
import Dashboard from './Dashboard';
import Element from '../components/PanelAdmin/Element';
import { Formik } from 'formik';
import { FaPlus } from 'react-icons/fa';
import {  Form, TextArea, Input, MyButton, Label, Header, Container, ContainerBox, ErrorTxt} from '../components/PanelAdmin/PanelAdminComponents';
import Actions from '../components/Posts/Actions';
import { connect } from 'react-redux';
import NotAdmin from './../components/PanelAdmin/NotAdmin';
import { Alert } from 'reactstrap';

const mapDispatchToProps = dispatch => ({
    add: (post) => dispatch(Actions.add(post)),
    reset: () => dispatch(Actions.reset())
  })


const mapStateToProps = state => ({
    titles: state.posts.Items.titles,
    login: state.login.Items,
    id_post: state.posts.Items.id_posts
})




class PanelAdmin extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            isFetching: false,
            response: '',
            responseAdd: '',
            responseErr: false,
            alertAddIsOpen: false,
            datas: []
        }
        this.getPosts = this.getPosts.bind(this);
    }

    async componentDidMount() {
        window.scroll(0, 0);
        this.setState({ isLoading: true });
        this.getPosts();
    }

    getPosts = async() => {
        await fetch('https://localhost:5001/Posts')
            .then (response => response.json())
            .then (data => this.setState({ isLoading: false, datas: data}))
            .catch(error => console.log('Error: ' + error))
            this.postsToRedux(this.state.datas);
    }
    
    postsToRedux = (data) => {
        console.log('data value: ', data);
        this.props.reset();
        data.map((el) => {
            const value = {
                id_post: el.id_post,
                title: el.title,
                content: el.content,
                date: el.datetime.substr(0,10)
            }
            this.props.add(value);
        })
    }

    checkAdmin = () => {
        const login = this.props.login;
        console.log("isAdmin: ", login.admin)
        if(login.admin) return true
        else return false
    }

    resetInput = (value) => {
        value.title = '';
        value.content = '';
        return value;
    }

    renderGoLogin() {

        return (
            <NotAdmin />
        );
    }

    onDismiss = () => {
        this.setState({alertAddIsOpen: false})
    }

    addPost(values) {
        this.setState({ responseAddErr: false, alertAddIsOpen: false})
        fetch('https://localhost:5001/Posts', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.login.token}`,
            },
            body: JSON.stringify({
                "title": values.title,
                "content": values.content,
            }),
        })
        .then((response) => {
            if(response.status !== 200) {
                throw new Error('Invalid HTTP status code: ' + response.status);
            }
            else {
                this.setState({ responseAdd: 'Post added successfully', alertAddIsOpen: true })
                this.getPosts();
        }
        })
        .catch((e) => this.setState({responseAdd: e.message ,responseAddErr: true, alertAddIsOpen: true}))
        this.resetInput(values);

    }

    renderPanelAdmin(){
        return (
            <Dashboard>
                <ContainerBox>
                    <Container>
                        <Header>Panel Admin</Header>
                        {this.props.titles.map((title, index) =>
                            <Element titleProp={title} id={index} id_post={this.props.id_post[index]} update={this.getPosts} />
                        )}
                        <Header>Add new post</Header>
                        <Formik 
                            initialValues={{ title: '', content: ''}}
                            onSubmit={ async (values) => this.addPost(values) }
                            validate={(values) => {
                                let errors = {};
                                if(!values.title) errors.title = 'Title is required!';
                                if(!values.content) errors.content = 'Content is required';
                                return errors;
                            }}
                            render={({
                                values,
                                errors,
                                touched,
                                handleBlur,
                                handleChange,
                                handleSubmit,
                                isSubmitting
                            }) => (
                                <Form onSubmit={handleSubmit}>
                                    <Label>Title: </Label>
                                    <Input 
                                        name='title' 
                                        onChange={handleChange}
                                        value={values.title}
                                        maxLength='70'
                                        />
                                    <ErrorTxt>{errors.title}</ErrorTxt>
                                    <Label>Content: </Label>
                                    <TextArea 
                                        name='content' 
                                        onChange={handleChange}
                                        value={values.content}
                                        />
                                    <ErrorTxt>{errors.content}</ErrorTxt>
                            {
                            this.state.alertAddIsOpen ? this.state.responseAddErr ? 
                            <Alert 
                                color="danger" 
                                isOpen={this.state.alertAddIsOpen} 
                                toggle={this.onDismiss}>
                                    {this.state.responseAdd}   
                            </Alert> 
                            : 
                            <Alert 
                                color="success" 
                                isOpen={this.state.alertAddIsOpen} 
                                toggle={this.onDismiss}>
                                    {this.state.responseAdd}
                            </Alert> 
                            :
                            undefined
                            }
                                    <MyButton type='submit' ><FaPlus color='white' size='20' /></MyButton>
                                </Form>            
                            ) 
                        }
                        />
                        <MyButton onClick={this.props.reset}>Reset ALL</MyButton>
                    </Container>
                </ContainerBox>
                
            </Dashboard>
        );
    }

    render() {
        return (
            <div>
                {this.checkAdmin() ? this.renderPanelAdmin() : this.renderGoLogin()}
            </div>
            
        );
        
        
    }
}



export default connect(mapStateToProps, mapDispatchToProps) (PanelAdmin);