import React, { Component } from 'react';
import Dashboard from './Dashboard';
import { Formik } from 'formik';
import { Button, Label, Header, LabelError } from './../components/Forms/FormComponents'; 
import FormContainer from './../components/Forms/FormContainer';
import './../styles/form.css';
import { Link } from 'react-router-dom';
import * as _ from 'ramda';
import { Alert } from 'reactstrap';
import Actions from '../components/Login/Actions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const mapDispatchToProps = dispatch => ({
    add: (login) => dispatch(Actions.add(login)),
    remove: () => dispatch(Actions.remove())
  })


const mapStateToProps = state => ({
    id_posts: state.posts.Items.id_posts,
    titles: state.posts.Items.titles,
    contents: state.posts.Items.contents,
    dates: state.posts.Items.dates
})

class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            disabled: false,
            alertIsOpen: false,
            isFetching: false,
            responseErr: false,
            redirect: false,
            login: {
                userName: '',
                token: '',
                admin: false
            }
        }
    }

    componentDidMount() {
        console.log('Sesja token: ', sessionStorage.getItem('token'));
        
    }


    resetInput = (value) => {
        value.name = '';
        value.password = '';
        value.email = '';
        return value;
    }

    refreshToken = token => {
        this.setState({token})
    }

    signin = async(values) => {
        console.log('Params: ', values.name, ' ', values.password);
        this.setState({ alertIsOpen: false, 
                        isFetching: false, 
                        responseErr: false,
                        redirect: false,
                        login: { userName: values.name }
                      });
         await fetch('https://localhost:5001/Authenticate', {
             method: 'POST',
             headers: {
                 'Content-Type': 'application/json',
                 'Accept': 'application/json',  
             },
             body: JSON.stringify({
                 "login": values.name,
                 "password": values.password,
             }),
        })
        .then(res => {
            if(res.status !== 200) {
                if(res.status === 401){
                    throw new Error('Incorrect login or password');
                }
                else throw new Error('Invalid HTTP status code: ' + res.status);
            }
            else {
                this.setState({response: 'Logged successfully ', alertIsOpen: true, isFetching: true});
                this.redirectToHome();
        }
        return res;
        })
        .then(response => response.json())
        .then(data => {
            console.log('data: ', data);
            this.setState({ login: { token: data.token, admin: data.admin, userName: this.state.login.userName}});
            this.props.add(this.state.login);
            this.addToSession(this.state.login);
            
        })
        .catch((e) => {
            console.log('err: ', e);
            this.setState({response: e.message, isFetching: true ,responseErr: true, alertIsOpen: true});
        })
    }

    redirectToHome = () => {
        setTimeout(() => {
            this.setState({ redirect: true})
        },3000)
    }

    addToSession = (values) => {
        sessionStorage.clear();
        sessionStorage.setItem('token', values.token);
        sessionStorage.setItem('userName', values.userName);
        sessionStorage.setItem('admin', values.admin);
    }

    render() {
        if(this.state.redirect){
            return(<Redirect to={'/'} />)
            
        }
        return(
            <Dashboard>
                <FormContainer>
                        <Header>Sign In</Header>
                        <Formik 
                        initialValues={{ name: '', email: '', password: '' }}
                        onSubmit={ async (values) => {
                            console.log('name: ' + values.name, 'password: ' + values.password);
                            this.signin(values);

                           // this.props.add(loginValue);
                            this.resetInput(values);
                        }}
                            validate={(values) => {
                                let errors = {}
                                let format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?1234567890]/;

                                if(!values.name) errors.name = 'Name is required!'
                                    else if (values.name.length < 3) errors.name = 'Min length is 3'
                                    else if (format.test(values.name)) errors.name = 'Invalid characters in field'

                                if(!values.password) errors.password = 'Password is required!'
                                    else if (values.password.length < 5) errors.password = 'Min length is 5'
                                
                                if(_.isEmpty(errors)) this.setState({disabled: false})
                                    else this.setState({disabled: true})

                                return errors
                            }} 
                            render={({
                                values,
                                errors,
                                touched,
                                handleBlur,
                                handleChange,
                                handleSubmit,
                                isSubmitting
                            }) => (
                                <form className='form' onSubmit={handleSubmit}>
                                    <Label>Login: </Label>
                                    <input 
                                        className="input" 
                                        name='name' 
                                        onChange={handleChange}
                                        value={values.name}
                                        maxLength='26'
                                        autoFocus
                                        />
                                    <LabelError className='errTxt'>{errors.name}</LabelError>
                                    <Label>Password: </Label>
                                    <input 
                                        className="input"
                                        name='password' 
                                        type='password'
                                        onChange={handleChange}
                                        value={values.password}
                                        maxLength='26'
                                        />
                                    <LabelError className='errTxt'>{errors.password}</LabelError>
                                    <p>Are you new? <Link className='linkForm' to='/signup'> Sign up now</Link></p>
                                    {
                                        this.state.isFetching ? this.state.responseErr ? 
                                        <Alert 
                                            color="danger" 
                                            isOpen={this.state.alertIsOpen} 
                                            toggle={this.onDismiss}>
                                                {this.state.response}   
                                        </Alert> 
                                        : 
                                        <Alert 
                                            color="success" 
                                            isOpen={this.state.alertIsOpen} 
                                            toggle={this.onDismiss}>
                                                {this.state.response} Redirecting in 3sec
                                        </Alert> 
                                        :
                                        undefined
                                    }
                                    <Button type='submit' disabled={this.state.disabled} >Sign In</Button>
                                </form>
                            )
                        }
                        />
                    </FormContainer>
            </Dashboard>
            
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (Signin);
