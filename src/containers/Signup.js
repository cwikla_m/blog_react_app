import React, { Component } from 'react';
import Dashboard from './Dashboard';
import { Formik } from 'formik';
import { Button, Label, Header, LabelError } from './../components/Forms/FormComponents'; 
import FormContainer from './../components/Forms/FormContainer';
import { Link } from 'react-router-dom';
import { Alert } from 'reactstrap';
import * as _ from 'ramda';


class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            alertIsOpen: false,
            isFetching: false,
            response: '',
            responseErr: false
        }
    }

    resetInput = (values) => {
        values.name = '';
        values.password = '';
        values.email = '';
        return values;
    }

   signup = async(values) => {
       this.setState({alertIsOpen: false, isFetching: false, responseErr: false})
        await fetch('https://localhost:5001/Registration', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "login": values.name,
                "password": values.password,
                "email": values.email,
            }),
        })
        .then((response) => {
            if(response.status !== 200) {
                if(response.status === 409 || response.status === 422){
                    throw new Error('User Login is already exist')
                }
                else throw new Error('Invalid HTTP status code: ' + response.status);              
            }
            else {
                this.setState({response: 'Registration successfully ', alertIsOpen: true, isFetching: true});
        }
        })
        .catch((e) => this.setState({response: e.message, isFetching: true ,responseErr: true, alertIsOpen: true}))
    }

    render() {
        
        return(
            <Dashboard>
                <FormContainer>
                <Header>Sign Up</Header>
                        <Formik 
                        initialValues={{ name: '', email: '', password: '' }}
                        onSubmit={ async (values) => {
                            console.log('name: ' + values.name, 'email: ', values.email, 'password: ' + values.password);
                            this.signup(values);
                            this.resetInput(values);
                        }}
                        validate={(values) => {
                            let errors = {}
                            let format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?1234567890]/;
                            let formatE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,4}))$/;

                            if(!values.name) errors.name = 'Name is required!'
                                else if (values.name.length < 3) errors.name = 'Min length is 3'
                                else if (format.test(values.name)) errors.name = 'Invalid Name!'

                            if(!values.password) errors.password = 'Password is required!'
                                else if (values.password.length < 5) errors.password = 'Min length is 5'

                            if(!values.email) errors.email = 'E-mail is requred!'
                                else if (values.email.length < 6) errors.email = 'Min length is 6'
                                else if (!formatE.test(values.email)) errors.email = 'Invalid E-mail!'
                            
                            if(_.isEmpty(errors)) this.setState({disabled: false})
                                else this.setState({disabled: true})

                            return errors
                        }} 
                            render={({
                                values,
                                errors,
                                touched,
                                handleBlur,
                                handleChange,
                                handleSubmit,
                                isSubmitting
                            }) => (
                                <form className='form' onSubmit={handleSubmit}>
                                    <Label>Login: </Label>
                                    <input 
                                        className="input"
                                        name='name' 
                                        onChange={handleChange}
                                        value={values.name}
                                        maxLength='26'
                                        autoFocus
                                        />
                                    <LabelError className='errTxt'>{errors.name}</LabelError>
                                    <Label>E-mail: </Label>
                                    <input 
                                        className="input"
                                        name='email' 
                                        onChange={handleChange}
                                        value={values.email}
                                        maxLength='26'
                                        />
                                    <LabelError className='errTxt'>{errors.email}</LabelError>
                                    <Label>Password: </Label>
                                    <input 
                                        className="input"
                                        name='password' 
                                        type='password'
                                        onChange={handleChange}
                                        value={values.password}
                                        maxLength='26'
                                        />
                                    <LabelError className='errTxt'>{errors.password}</LabelError>
                                    <p>Do you already have an account? <Link className='linkForm' to='/signin'> Sign in</Link></p>
                                    {
                                        this.state.isFetching ? this.state.responseErr ? 
                                        <Alert 
                                            color="danger" 
                                            isOpen={this.state.alertIsOpen} 
                                            toggle={this.onDismiss}>
                                                {this.state.response}   
                                        </Alert> 
                                        : 
                                        <Alert 
                                            color="success" 
                                            isOpen={this.state.alertIsOpen} 
                                            toggle={this.onDismiss}>
                                                {this.state.response}
                                        </Alert> 
                                        :
                                        undefined
                                    }
                                    <Button type='submit'>Sign Up</Button>
                                </form>
                            )
                        }
                        />
                    </FormContainer>
            </Dashboard>
            
        );
    }
}

export default Signup;
