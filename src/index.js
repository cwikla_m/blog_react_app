import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import './styles/index.css';
import './styles/reactStrapStyles.css';
import { Provider } from 'react-redux';
import store from './components/Store';

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
 document.getElementById('root'));

